# ytsdl




#ytsdl

ytsdl is a small script which uses youtube-dl and youtubepy to search and download videos from youtube. Install requirements:

``` pip install youtube-dl ```

``` pip install youtubepy ```

Make sure ytsdl.py is executable on linux systems

``` chmod +x ytsdl.py ```

Execute script:

``` ./ytsdl.py ```

Have fun!
