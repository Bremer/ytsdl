#!/usr/bin/python3
""" Script to search and download content from youtube """

import subprocess
from shutil import which
from youtubepy import Video


def search_video(search_for_this):
    """ Search for content on youtube """
    video = Video(search_for_this)
    try:
        search_result = video.search()
    except ValueError:
        return None, None
    title_of_video = video.title()
    return search_result, title_of_video


def download_video(video_to_download):
    """ Download content from youtube """
    if which('youtube-dl'):
        subprocess.run(['youtube-dl', video_to_download], check=True)
    else:
        print('Sorry, youtube-dl not installed')
        return


if __name__ == "__main__":
    search_string = input("Insert search string: ")
    result, title = search_video(search_string)
    while True:
        if result is None:
            print('No search result!')
            print('Good bye!')
            break
        print(title)
        answer = input("download video? yY/nN: ")
        if answer.lower() == 'y':
            download_video(result)
            break
        if answer.lower() == 'n':
            print("Good bye!")
            break
        print("Please specify yY or nN")
